﻿component extends="mura.plugin.pluginGenericEventHandler" {

   public any function dspAccordions($){
      var params=$.event('objectParams');
      params.tab='';
      var str='';

      // default feedID
      if(not structKeyExists(params,"feedID")){
         params.feedID='';
      }

      // accordion title
      if(not structKeyExists(params,"accordionTitle")){
         params.accordionTitle='';
      }

      // // accordion nav options
      // if(not structKeyExists(params,"accordionContainerID")){
      //    params.accordionContainerID='';
      // }

      // if(not structKeyExists(params,"accordionContainerClass")){
      //    params.accordionContainerClass='';
      // }

      // accordion layout
      if (len(params.feedID)){

         // begin accordion loop
         accordionFeed=$.getBean("feed").loadBy(feedID=params.feedID).getIterator();

         if (accordionFeed.hasNext()){
            str='';

            if ( params.accordionTitle neq '') {
               str=str & '<#$.getHeaderTag('subHead1')#>#htmlEditFormat(params.accordionTitle)#</#$.getHeaderTag('subHead1')#>';
            };

            str=str & '<div class="accordion" id="#htmlEditFormat(params.feedID)#">';

            // if ( params.accordionContainerID neq '') {
            //    str=str & 'id="#htmlEditFormat(params.accordionContainerID)#"';
            // }

            // if ( params.accordionContainerClass neq '') {
            //    str=str & 'id="#htmlEditFormat(params.accordionContainerClass)#"';
            // }

            while(accordionFeed.hasNext()){

               accordionFeedItem=accordionFeed.next();

               str=str & '<div class="accordion-group">
                  <div class="accordion-heading">
                     <h3><a class="accordion-toggle" data-toggle="collapse" data-parent="###htmlEditFormat(params.feedID)#" href="##accordion_#accordionFeedItem.getContentID()#">#accordionFeedItem.getMenuTitle()#</a></h3>
                  </div>

                  <div id="accordion_#accordionFeedItem.getContentID()#" class="accordion-body collapse';

               // if ( accordionFeed.currentIndex() == 1) {
               //    str=str & ' in"';
               // };

               str=str & '" >
                        <div class="accordion-inner">
                           #accordionFeedItem.getBody()#
                        </div>
                     </div>
                  </div>';
            } // end accordion loop

            str=str & '</div>';

         } // end accordion feed loop

      } //end accordion layout

       // Include CSS to Header
       // pluginConfig.addToHtmlHeadQueue('displayObjects/htmlhead.cfm');

       // Include JS to footer
       // pluginConfig.addToHtmlFootQueue('displayObjects/htmlfoot.cfm');

      return str;

   }//end dspAccordions

   // tabs
   public any function dspTabs($){
      var params=$.event('objectParams');
      params.tab='';
      var str='';

      // default feedID
      if(not structKeyExists(params,"feedID")){
         params.feedID='';
      }

      // tab title
      if(not structKeyExists(params,"tabTitle")){
         params.tabTitle='';
      }

      // // tab nav options
      // if(not structKeyExists(params,"tabNavID")){
      //    params.tabNavID='';
      // }

      // if(not structKeyExists(params,"tabNavClass")){
      //    params.tabNavClass='';
      // }

      // // tab panel options
      // if(not structKeyExists(params,"tabPanelID")){
      //    params.tabPanelID='';
      // }

      // if(not structKeyExists(params,"tabPanelClass")){
      //    params.tabPanelClass='';
      // }

      // tab layout
      if (len(params.feedID)){

         // begin tab nav loop

         // get list for tabs
         tabNav=$.getBean("feed").loadBy(feedID=params.feedID).getIterator();

         if (tabNav.hasNext()){

            str='';

            if ( params.tabTitle neq '') {
               str=str & '<#$.getHeaderTag('subHead1')#>#htmlEditFormat(params.tabTitle)#</#$.getHeaderTag('subHead1')#>';
            };

            str=str & '<ul ';

            // if ( params.tabNavID neq '') {
            //    str=str & 'id="#htmlEditFormat(params.tabNavID)#"';
            // }

            str=str & 'class="nav nav-tabs';

            // if ( params.tabNavClass neq '') {
            //    str=str & 'id="#htmlEditFormat(params.tabNavClass)#"';
            // }

            str=str & '" >';

            while(tabNav.hasNext()){

               tabNavItem=tabNav.next();

               str=str & '<li';

               if ( tabNav.currentIndex() eq 1) {
                  str=str & ' class="active" ';
               };

               str=str & '><a data-toggle="tab" href="##tab_#tabNavItem.getContentID()#">#tabNavItem.getMenuTitle()#</a></li>';
            }

            str=str & '</ul>';

         } // end tab navigation


         // begin tab panels loop

         // get list for tabs
         tabPanels=$.getBean("feed").loadBy(feedID=params.feedID).getIterator();

         str=str & '<div class="tab-content">';

         if (tabPanels.hasNext()){

            while(tabPanels.hasNext()){
               tabNavItem=tabPanels.next();
               str=str & '<div class="tab-pane fade ';

               if ( tabPanels.currentIndex() eq 1) {
                  str=str & 'active in';
               }


               str=str & '" id="tab_#tabNavItem.getContentID()#">#tabNavItem.getSummary()#</div>';
            }

            str=str & '</div><hr>';

         } // end tab panels

      } //end tab layout


       // Include CSS to Header
       // pluginConfig.addToHtmlHeadQueue('displayObjects/htmlhead.cfm');

       // Include JS to footer
       // pluginConfig.addToHtmlFootQueue('displayObjects/htmlfoot.cfm');

      return str;

   }//end dspTabs


   public any function dspScaffolds($){
      var params=$.event('objectParams');
      var str='';

      // default feedID
      if(not structKeyExists(params,"feedID")){
         params.feedID='';
      }

      // default columns
      if(not structKeyExists(params,"col")){
         params.col='4';
      }
      
      // scaffold title
      if(not structKeyExists(params,"scaffoldTitle")){
         params.scaffoldTitle='';
      }

      switch(params.col){

         case 2:
         variables.span = 6;
         break;

         case 3:
         variables.span = 4;
         break;   

         case 4:
         variables.span = 3;
         break;

         case 6:
         variables.span = 2;
         break;

         case 1:2
         variables.span = 1;
         break;

         default:
         variables.span = 3;
         break;
      }



      // scaffold layout
      if (len(params.feedID)){


         // begin scaffold loop
         scaffoldFeed=$.getBean("feed").loadBy(feedID=params.feedID).getIterator();

         if (scaffoldFeed.hasNext()){
            str='<aside class="scaffold default">
                  <#$.getHeaderTag('subHead1')# class="headBG"><span>#htmlEditFormat(params.scaffoldTitle)#</span></#$.getHeaderTag('subHead1')#>
                  <div class="row-fluid">  
            ';

            while(scaffoldFeed.hasNext()){

               scaffoldFeedItem=scaffoldFeed.next();

               str=str & '<article class="span#htmlEditFormat(params.col)# default">'; //#scaffoldFeed.currentIndex()# MOD #params.col# =  #( scaffoldFeed.currentIndex() mod params.col )#

               if( len( scaffoldFeedItem.getImageURL()) ) {
                  str=str & '
                     <a href="#scaffoldFeedItem.getURL()#" title="Learn more about #scaffoldFeedItem.getTitle()#">
                        <figure class="top">
                           <img class="img-rounded" src="#scaffoldFeedItem.getImageURL(size='medium')#" alt="#scaffoldFeedItem.getTitle()#" title="#scaffoldFeedItem.getTitle()#" />
                        </figure>
                     </a>';
               };

               str=str & '
                  <header>
                     <h1><a href="#scaffoldFeedItem.getURL()#" title="Learn more about #scaffoldFeedItem.getTitle()#"> #scaffoldFeedItem.getMenuTitle()#</a></h1>
                  </header>
                  <hr>
               ';

               if( len(scaffoldFeedItem.getSummary()) ) {
                  str=str & '#scaffoldFeedItem.getSummary()#';
               }

               str=str & '      
                  </article>
               ';

               if ( scaffoldFeed.currentIndex() mod variables.span eq 0 ) {
                  str=str & '</div>';
               //    lseif ( scaffoldFeed.currentIndex() mod params.col eq 0  ) {
               //    str=str & '<div class="row-fluid">';
               // }


               };

            }; // end scaffold loop


            str=str & '</div></aside>';

         } // end scaffold feed loop

      } //end scaffold layout

       // Include CSS to Header
       // pluginConfig.addToHtmlHeadQueue('displayObjects/htmlhead.cfm');

       // Include JS to footer
       // pluginConfig.addToHtmlFootQueue('displayObjects/htmlfoot.cfm');

      return str;

   }//end dspscaffolds

}
