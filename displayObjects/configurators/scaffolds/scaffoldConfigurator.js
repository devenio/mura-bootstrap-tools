function initScaffoldsConfigurator(data){

	initConfigurator(
		data,
		{
			url:'../plugins/MuraBootstrapTools/displayObjects/configurators/scaffolds/scaffoldConfigurator.cfm',
			pars:'',
			title: 'MBT - Scaffolds',
			init: function(){},
			destroy: function(){},
			validate: function(){
				return validateForm(document.getElementById('availableObjectParamsForm'));
				}
		}
	);

	return true;
}