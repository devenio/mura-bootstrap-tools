﻿<cfscript>
	$=application.serviceFactory.getBean("MuraScope").init(session.siteID);

	params=$.event("params");

	if(isJSON(params)){
		params=deserializeJSON(params);
	} else {
		params=structNew();
	}

	if(not structKeyExists(params,"feedID")){
		params.feedID='';
	}

  // default columns
  if(not structKeyExists(params,"col")){
     params.col='4';
  }

	// scaffold title
	if(not structKeyExists(params,"scaffoldTitle")){
		params.scaffoldTitle='';
	}

	rsFeeds=$.getBean('feedManager').getFeeds(type='local',siteID=session.siteid);
</cfscript>
<cfoutput>
	<div id="availableObjectParams"
		data-object="plugin"
		data-name="MBT - Scafffold - #HTMLEditFormat(params.scaffoldTitle)#"
		data-objectid="#$.event('objectID')#">
		<form id="availableObjectParamsForm" onsubmit="return false;" style="display:inline;">

			<dl class="singleColumn">

				<dt>Local Index</dt>
				<dd><select name="feedID" class="objectParam" data-required="true" data-message="Please select a local index.">
					<option value="">-- Select Local index --</option>
					<cfloop query="rsFeeds">
						<option value="#rsFeeds.feedID#"<cfif params.feedID eq rsFeeds.feedID> selected</cfif>>#HTMLEditFormat(rsFeeds.name)#</option>
					</cfloop>
				</select></dd>

				<dt>Number of Columns</dt>
				<dd>
					<select name="col" class="objectParam" data-required="true" data-message="Please select a column size.">
						<option value="">-- Select Number of Columns --</option>
						<option value="6"<cfif params.col eq 2> selected</cfif>>2 Columns</option>
						<option value="4"<cfif params.col eq 4> selected</cfif>>3 Columns</option>
						<option value="3"<cfif params.col eq 3> selected</cfif>>4 Columns</option>
						<option value="2"<cfif params.col eq 6> selected</cfif>>6 Columns</option>
						<option value="1"<cfif params.col eq 12> selected</cfif>>12 Columns</option>
					</select>
				</dd>

				<dt>Scaffold Title <small>(optional: shows up above the columns)</small></dt>
				<dd><input name="scaffoldTitle" class="objectParam  text" data-required="true" value="#HTMLEditFormat(params.scaffoldTitle)#" /></dd>
			</dl>

		</form>
	</div>
</cfoutput>