function initAccordionsConfigurator(data){

	initConfigurator(
		data,
		{
			url:'../plugins/MuraBootstrapTools/displayObjects/configurators/accordions/accordionConfigurator.cfm',
			title: 'MuraBootstrap Tools - Accordions',
			init: function(){},
			destroy: function(){},
			validate: function(){
				return validateForm(document.getElementById('availableObjectParamsForm'));
				}
		}
	);

	return true;
}