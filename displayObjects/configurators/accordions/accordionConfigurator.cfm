﻿<cfscript>
	$=application.serviceFactory.getBean("MuraScope").init(session.siteID);

	params=$.event("params");

	if(isJSON(params)){
		params=deserializeJSON(params);
	} else {
		params=structNew();
	}

	if(not structKeyExists(params,"feedID")){
		params.feedID='';
	}

	// accordion title
	if(not structKeyExists(params,"accordionTitle")){
		params.accordionTitle='';
	}

	// // accordion options
	// if(not structKeyExists(params,"accordionNavID")){
	// 	params.accordionNavID='';
	// }

	// if(not structKeyExists(params,"accordionNavClass")){
	// 	params.accordionNavClass='';
	// }


	rsFeeds=$.getBean('feedManager').getFeeds(type='local',siteID=session.siteid);
</cfscript>
<cfoutput>
	<div id="availableObjectParams"
		data-object="plugin"
		data-name="MBT - Accordion - #HTMLEditFormat(params.accordionTitle)#"
		data-objectid="#$.event('objectID')#">
		<form id="availableObjectParamsForm" onsubmit="return false;" style="display:inline;">

			<dl class="singleColumn">

				<dt>Local Index</dt>
				<dd><select name="feedID" class="objectParam" data-required="true" data-message="Please select a local index.">
					<option value="">-- Select Local index --</option>
					<cfloop query="rsFeeds">
						<option value="#rsFeeds.feedID#"<cfif params.feedID eq rsFeeds.feedID> selected</cfif>>#HTMLEditFormat(rsFeeds.name)#</option>
					</cfloop>
				</select></dd>

				<dt>Accordion Title for Panel <small>(optional: shows up above the accordions)</small></dt>
				<dd><input name="accordionTitle" class="objectParam  text" data-required="false" value="#HTMLEditFormat(params.accordionTitle)#" /></dd>

<!--- 				<dt>accordion Navigation ID <small>(optional: use this as a hook for the navigation ID, if needed)</small></dt>
				<dd><input name="accordionNavID" class="objectParam  text" data-required="false"  value="#HTMLEditFormat(params.accordionNavID)#" /></dd>

				<dt>accordion Navigation Class <small>(optional: use this as a hook for the navigation class, if needed)</small></dt>
				<dd><input name="accordionNavClass" class="objectParam  text" data-required="false" value="#HTMLEditFormat(params.accordionNavClass)#" /></dd>

				<dt>accordion Panel ID <small>(optional: use this as a hook for the ID of each accordion panel, if needed)</small></dt>
				<dd><input name="accordionNavID" class="objectParam  text" data-required="false" value="#HTMLEditFormat(params.accordionNavID)#" /></dd>

				<dt>accordion Panel Class <small>(optional: use this as a hook class of each accordion panel, if needed)</small></dt>
				<dd><input name="accordionNavClass" class="objectParam  text" data-required="false" value="#HTMLEditFormat(params.accordionNavClass)#" /></dd>
 --->
			</dl>

		</form>
	</div>
</cfoutput>