﻿<cfscript>
	$=application.serviceFactory.getBean("MuraScope").init(session.siteID);

	params=$.event("params");

	if(isJSON(params)){
		params=deserializeJSON(params);
	} else {
		params=structNew();
	}

	if(not structKeyExists(params,"feedID")){
		params.feedID='';
	}

	// tab title
	if(not structKeyExists(params,"tabTitle")){
		params.tabTitle='';
	}

	// // tab nav options
	// if(not structKeyExists(params,"tabNavID")){
	// 	params.tabNavID='';
	// }

	// if(not structKeyExists(params,"tabNavClass")){
	// 	params.tabNavClass='';
	// }

	// // tab panel options
	// if(not structKeyExists(params,"tabPanelID")){
	// 	params.tabPanelID='';
	// }

	// if(not structKeyExists(params,"tabPanelClass")){
	// 	params.tabPanelClass='';
	// }

	rsFeeds=$.getBean('feedManager').getFeeds(type='local',siteID=session.siteid);
</cfscript>
<cfoutput>
	<div id="availableObjectParams"
		data-object="plugin"
		data-name="Mura Bootstrap Tools - Tabs"
		data-objectid="#$.event('objectID')#">
		<form id="availableObjectParamsForm" onsubmit="return false;" style="display:inline;">

			<dl class="singleColumn">

				<dt>Local Index</dt>
				<dd><select name="feedID" class="objectParam" data-required="true" data-message="Please select a local index.">
					<option value="">-- Select Local index --</option>
					<cfloop query="rsFeeds">
						<option value="#rsFeeds.feedID#"<cfif params.feedID eq rsFeeds.feedID> selected</cfif>>#HTMLEditFormat(rsFeeds.name)#</option>
					</cfloop>
				</select></dd>

				<dt>Tab Title for Panel <small>(optional: shows up above the tabs)</small></dt>
				<dd><input name="tabTitle" class="objectParam  text" data-required="false" value="#HTMLEditFormat(params.tabTitle)#" /></dd>

<!--- 				<dt>Tab Navigation ID <small>(optional: use this as a hook for the navigation ID, if needed)</small></dt>
				<dd><input name="tabNavID" class="objectParam  text" data-required="false"  value="#HTMLEditFormat(params.tabNavID)#" /></dd>

				<dt>Tab Navigation Class <small>(optional: use this as a hook for the navigation class, if needed)</small></dt>
				<dd><input name="tabNavClass" class="objectParam  text" data-required="false" value="#HTMLEditFormat(params.tabNavClass)#" /></dd>

				<dt>Tab Panel ID <small>(optional: use this as a hook for the ID of each tab panel, if needed)</small></dt>
				<dd><input name="tabNavID" class="objectParam  text" data-required="false" value="#HTMLEditFormat(params.tabNavID)#" /></dd>

				<dt>Tab Panel Class <small>(optional: use this as a hook class of each tab panel, if needed)</small></dt>
				<dd><input name="tabNavClass" class="objectParam  text" data-required="false" value="#HTMLEditFormat(params.tabNavClass)#" /></dd>
 --->
			</dl>

		</form>
	</div>
</cfoutput>