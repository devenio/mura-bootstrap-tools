function initTabsConfigurator(data){

	initConfigurator(
		data,
		{
			url:'../plugins/MuraBootstrapTools/displayObjects/configurators/tabs/tabConfigurator.cfm',
			pars:'',
			title: 'MuraBootstrap Tools - Tabs',
			init: function(){},
			destroy: function(){},
			validate: function(){
				return validateForm(document.getElementById('availableObjectParamsForm'));
				}
		}
	);

	return true;
}